import {ReactComponent as StarIcon} from "../../img/star-fill.svg"
import {ReactComponent as BasketIcon} from "../../img/basket.svg"
import PropTypes from "prop-types";
import styles from "./Header.module.css"
import {Link} from "react-router-dom";

function Header({favLength, cardLength}) {
    return <header className={styles.header}>
        <nav>
            <ul className={styles.list}>
                <li className={styles.item}>
                    <Link to="/">Home</Link>
                </li>
                <li className={styles.item}>
                    <Link to="/favourite">Favourite<StarIcon className={styles.starIcon}/>{favLength}</Link>
                </li>
                <li className={styles.item}>
                    <Link to="/basket">Basket <BasketIcon className={styles.bascetIcon}/>{cardLength}</Link>
                </li>
            </ul>
        </nav>
    </header>
}

Header.propTypes = {favLength: PropTypes.number, cardLength: PropTypes.number}
export default Header;
