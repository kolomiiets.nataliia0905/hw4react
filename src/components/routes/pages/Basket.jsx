import {useOutletContext} from "react-router-dom";
import List from "../../list/List";
import {useSelector} from "react-redux";
import EmptyListChecker from "../Components/EmptyListChecker";

function Basket() {
    const {basket,products} = useSelector(store=>store)
    return (
        <div>
            <EmptyListChecker checkArr={basket} message={"Empty basket list!"}>
                <List cardClick={true}
                      data={products.filter(product =>basket.includes(product.id))}/>
            </EmptyListChecker>

        </div>
    );
}
export default Basket;