import List from "../../list/List";
import {useSelector} from "react-redux";
import EmptyListChecker from "../Components/EmptyListChecker";

function Favourite() {
    const {favorite,products} = useSelector(store=>store)
    return (
        <div>
            <EmptyListChecker checkArr={favorite} message={"Empty favorite list!"}>
                <List
                      data={products.filter(product =>favorite.includes(product.id))}/>
            </EmptyListChecker>

        </div>
    );
}

export default Favourite;