export default function EmptyListChecker({children, message, checkArr}) {

    if (checkArr.length) {
        return children
    } else {
        return <h2>{message}</h2>
    }

}