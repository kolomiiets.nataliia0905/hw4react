import { Routes, Route, Link } from "react-router-dom";
import Layout from "./Layout";
import Home from "../pages/Home";
import Favourite from "../pages/Favourite";
import Basket from "../pages/Basket";
import NoMatch from "../pages/NoMatch";

function UserRoutes() {
    return  <Routes>
        <Route path="/" element={<Layout />}>
            <Route index element={<Home />} />
            <Route path="favourite" element={<Favourite/>} />
            <Route path="basket" element={<Basket/>} />
            <Route path="*" element={<NoMatch />} />
        </Route>
    </Routes>
};
export default UserRoutes;